package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class MinMaxTest {

    @Test
    public void testF() {
        // Arrange
        MinMax minMax = new MinMax();

        // Act
        int result = minMax.f(5, 10);

        // Assert
        assertEquals(10, result);
    }

    @Test
    public void testBar() {
        // Arrange
        MinMax minMax = new MinMax();

        // Act
        String result = minMax.bar("test");

        // Assert
        assertEquals("test", result);
    }

    @Test
    public void testBarWithNull() {
        // Arrange
        MinMax minMax = new MinMax();

        // Act
        String result = minMax.bar(null);

        // Assert
        assertEquals("", result);
    }
}